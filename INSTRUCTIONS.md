# Instructions
## Start the demo stack
Run `docker-compose up` to build and start all the dependent services!

## Access User Accounts and Authentication server (UAA)
1. Install the UAA-CLI [instructions here](https://docs.cloudfoundry.org/uaa/uaa-user-management.html).
2. Target the local UAA instance.
    ```shell script
    $ uaa target http://uaa.local:8080/uaa
    ```
3. Get a client token as the `admin` client to administer users
     ```sh
    $ uaa get-client-credentials-token admin -s <client_secret>
     ```
    By default, the admin client secret is just `adminsecret`.
4. Check access using a command such as
    ```shell script
    $ uaa list-users
    ```
   You should see some sensible output here with the default demo users registered in UAA.

## Add a user
1. Use the UAA-CLI command:
    ```shell script
    $ uaa create-user ada \
         --email test@example.com \
         --familyName lovelace \
         --givenName ada \
         --password password123
    ```
2. You should now be able to login as that user by visiting `http://uaa.local:8080/uaa` in a web-browser.

## Check authentication to resource server (frontend app in this case)
1. Visit http://localhost:8080/accounts/ in a browser
2. Login as user (if you hadn't previously)
3. You should see the JWT token which was passed to the JS server