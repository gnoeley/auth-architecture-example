#!/bin/bash

# Replace placeholder variables in file using env_var
# Use awk (not sed) to handle replace with multi-line variables
# Accepts an optional indent when dealing with multi-line values (e.g. yaml block-scalars)
function replace_placeholder {
  local file=$1
  local indent=$(printf "%*s" "${4:-0}" '')
  local replace=$(echo "${!3}" | sed "s/^/$indent/")

  awk -v placeholder="$2" \
      -v replace="$replace" \
      '{sub(placeholder,replace)}1' \
      "${file}" > "${file}".tmp

  mv "${file}".tmp "${file}"
}

# Replace secure variables in UAA config file
# See issue https://github.com/cloudfoundry/uaa/issues/1281
UAA_CONFIG_FILE=${CLOUD_FOUNDRY_CONFIG_PATH}/uaa.yml
replace_placeholder "${UAA_CONFIG_FILE}" "#{LOGIN_SAML_KEY}" "LOGIN_SAML_KEY" 10
replace_placeholder "${UAA_CONFIG_FILE}" "#{LOGIN_SAML_PASSPHRASE}" "LOGIN_SAML_PASSPHRASE"
replace_placeholder "${UAA_CONFIG_FILE}" "#{LOGIN_SAML_CERTIFICATE}" "LOGIN_SAML_CERTIFICATE" 10

# Start Tomcat server
/usr/local/tomcat/bin/catalina.sh run

