import { useEffect, useState } from 'react'
import Head from 'next/head'
import styles from '../styles/Home.module.css'

export default function Home() {

  const [jwt, setJwt] = useState({})
  useEffect(() => {
    fetch('/accounts/api/echo-jwt', { credentials: 'same-origin' })
        .then(res => res.status === 200 && res.json())
        .then(json => setJwt(json.jwt))
  }, [])

  console.log(jwt)

  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1>
          This is the JWT token which would be passed to a resouce server
        </h1>

        <div>
          <pre>
            { JSON.stringify(jwt, null, 2) }
          </pre>
        </div>

      </main>
    </div>
  )
}
