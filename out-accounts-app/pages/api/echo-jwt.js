import jwt from 'jsonwebtoken'
import jwksClient from 'jwks-rsa/lib'

// Uses https://github.com/auth0/node-jwks-rsa
// and https://github.com/auth0/node-jsonwebtoken
export default (req, res) => {
  console.log(req.headers)

  const client = jwksClient({
    strictSsl: false,
    jwksUri: 'http://uaa.local:8080/uaa/token_keys'
  });

  const kid = 'legacy-token-key'
  const getKey = (header, callback) => {
    client.getSigningKey(kid, function(err, key) {
      var signingKey = key.publicKey || key.rsaPublicKey;
      callback(null, signingKey);
    });
  }

  const token = req.headers.authorization.slice(7)
  jwt.verify(token, getKey, {}, function(err, decoded) {
    console.log("Your JWT token: \n" + decoded)
    res.statusCode = 200
    res.json({ jwt: decoded })
  });
}
