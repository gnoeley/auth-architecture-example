#TODOs
##UAA Server
- Secure Tomcat (https://www.upguard.com/blog/15-ways-to-secure-apache-tomcat-8)
- Configure UAA server to not expose registration/account pages
- Don't use default profile (to avoid creating dummy users)
- Create an admin client for CLI access
- Create a gateway client for OAuth grant flows
- Figure out deployment strategy (docker/k8/ansible)
- Certificate and signing key creation
- HTTPS for inter-service communication

## API Gateway
- Externalise secrets into env_vars

## Account API
- Present a registration page