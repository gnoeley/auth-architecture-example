# Authentication Architecture Concept
This project contains a few services which form an authentication architecture based on OAuth 2.0.

By containing all authentication and account concerns in one system (UAA) we remain flexible in terms of how downstream (aka resource) services are implemented.
The Gateway is not strictly necessary as clients could directly interact with the UAA server. Having a gateway however allows for all routes to be configured, secured and publicly exposed from a single service allowing for easier management.

## How it would work
This diagram shows the proposed flow for a registration journey
![registration architecture diagram](Auth%20Architecture.png)

The authentication and account management is provided by a Cloud Foundry UAA server.
An API Gateway (Spring Cloud Gateway) then receives client requests, delegating SSO to the authentication server.
Once the gateway receives the auth response it requests a token which it uses to request the user information in the form of a JWT token.

The JWT token is passed in the `Authorization` header to downstream services which they can use to verify user identity and scopes/authorities.

This is a work-in-progress and requires more work before it would be production ready (see [TODO.md](TODO.md)).

For instructions on running locally see [INSTRUCTIONS.md](INSTRUCTIONS.md)

## References
- https://docs.cloudfoundry.org/concepts/architecture/uaa.html
- https://github.com/cloudfoundry/uaa/tree/develop/docs
- https://jwt.io/
- https://spring.io/blog/2019/08/16/securing-services-with-spring-cloud-gateway
- https://hub.docker.com/_/tomcat
- https://docs.cloudfoundry.org/api/uaa/version/4.8.0/propMappings/
- https://github.com/cloudfoundry/uaa-release/blob/v40/jobs/uaa/spec#L710
- https://github.com/cloudfoundry-incubator/uaa-cli
